pipelineJob('pipelineJob') {
    definition {
        cps {
            script(readFileFromWorkspace('pipelineJob.groovy'))
            sandbox()
        }
    }
}

pipelineJob('car-insurance') {
    definition {
        cpsScm {
            scm {
                git {
                    remote {
                        url 'https://gitlab.com/p.h.pavlov/carinsurancecalcapp.git'
                    }
                    branch 'master'
                }
            }
        }
    }
}